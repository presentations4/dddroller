============
Introduction
============

Installation
============

Python Package Index repository
-------------------------------

Works on python >= 3.8
Download `Python 3.8 <https://www.python.org/downloads/release/python-38>`_

Install it directly from the public pypi repository

``pip install dddroller``

or download the tar.gz artifact and install locally

``pip install /path/to/<artifact>.tar.gz``

Git
---
clone the project from gitlab

``git clone https://gitlab.com/presentations4/dddroller.git``

Or download the raw files `here <https://gitlab.com/presentations4/dddroller/-/archive/master/dddroller-master.tar.gz>`_ 

Install poetry on your system.

``pip install poetry``

And then install it using poetry

``poetry install``

.. caution:: *this method does imply that you will be running the system with poetry*

Usage
=====

Using python >= 3.8 execute 

Python
------

If you retrieved it via pypi the package from **pypi** and installed it with **pip**

``python -m dddroller <roll instruction>``

Poetry
------
If you retrieved it via **git** and installed it using **poetry**

``poetry run python -m dddroller <roll instruction>``


API
---

``dddroller <--flag(s)> <instruction>``

    * -h --help:    help flag, runs application/command with its help page
    * -v --verbose: verbose flag, increases the applications log verbosity
    * instructions: instruction(s) set for dice roll(s), **at least one is required**
    
Instruction
^^^^^^^^^^^

4d6b3+2

.. line-block::

    **4** ->    how many dice
    **d6** ->   what type of dice, required
    **b3** ->   which of the values to pick; 'b' = 'best' or 'w' = 'worst'.
    **+2** ->   modifications to be done to the total value; negative number is allowed.
    
.. tip:: instructions without a leading digid will be counted as an one roll


=====================
Contributute/Workshop
=====================

Requirements
============

- Container Management Tool
    - Docker
    - Podman
- Visual Studio Code
- Internet

    *You could run it without VS Code, but then you will be manually setting your enviroment*

Setup
=====
