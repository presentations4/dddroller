# -*- coding: utf-8 -*-
import pytest

from dddroller import (
    BEST,
    DIE,
    MODIFIER,
    PICKS,
    QUANTITY,
    QUOTA,
    ROLLS,
    PICK_DEFAULT,
)

from dddroller.utilities import (
    instruction_builder,
    instruction_parser,
    roll_dices,
    cherrypick_roll,
)


@pytest.mark.parametrize(
    "expected, instructions",
    [
        ("2d10+5", (10, 2, 2, True, 5)),
        ("d10-3", (10, 1, 0, False, -3)),
        ("4d6B3+1", (6, 4, 3, True, 1)),
        (
            "6d8W3-2",
            (
                8,
                6,
                3,
                False,
                -2,
            ),
        ),
        ("d6-3", (6, 1, 0, True, -3)),
        ("d6", (6, 1, 0, False, 0)),
        ("d100", (100, 1, 0, True, 0)),
        ("10d8-5", (8, 10, 0, True, -5)),
    ],
)
def test_instruction_builder(expected, instructions) -> None:
    assert instruction_builder(*instructions) == expected


@pytest.mark.parametrize(
    "instruction, expected",
    [
        ("2d10+5", ({QUANTITY: 2, DIE: 10}, PICK_DEFAULT, 5)),
        ("d10-3", ({QUANTITY: 1, DIE: 10}, PICK_DEFAULT, -3)),
        (
            "4d6b3+1",
            (
                {QUANTITY: 4, DIE: 6},
                {BEST: True, QUOTA: 3},
                1,
            ),
        ),
        (
            "6d8w3-2",
            (
                {QUANTITY: 6, DIE: 8},
                {BEST: False, QUOTA: 3},
                -2,
            ),
        ),
        ("1d6-3", ({QUANTITY: 1, DIE: 6}, PICK_DEFAULT, -3)),
        ("d6", ({QUANTITY: 1, DIE: 6}, PICK_DEFAULT, 0)),
        ("d100", ({QUANTITY: 1, DIE: 100}, PICK_DEFAULT, 0)),
        ("10d8-5", ({QUANTITY: 10, DIE: 8}, PICK_DEFAULT, -5)),
    ],
)
def test_instruction_parser(instruction, expected) -> None:
    assert instruction_parser(instruction=instruction) == expected


@pytest.mark.parametrize(
    "instruction",
    [
        "10-3",
        "t8+5",
        "d21",
        "2d10b3",
        "2d10z1",
        "d10*2",
        "d10/2",
        "d10^2",
    ],
)
def test_instruction_parser_value_error(instruction):
    with pytest.raises(ValueError):
        instruction_parser(instruction=instruction)


@pytest.mark.parametrize(
    "quantity, die, upper_bound",
    [
        (2, 10, 10),
        (1, 6, 6),
        (1, 100, 100),
        (2, 100, 100),
    ],
)
def test_roll_dices(quantity, die, upper_bound) -> None:
    for value in roll_dices(quantity=quantity, die=die):
        assert 0 < value <= upper_bound


@pytest.mark.parametrize(
    "rolls, best, quota, expected",
    [
        ([15, 3, 94, 1, 12, 54, 10], True, 3, (94, 54, 15)),
        ([15, 3, 94, 1, 12, 54, 10], False, 3, (1, 3, 10)),
        ([1, 3, 2], True, 3, (3, 2, 1)),
        ([1, 3, 2], True, 0, (3, 2, 1)),
        ([6, 8, 4], False, 3, (4, 6, 8)),
        ([2, 4], True, 2, (2, 4)),
        ([2, 4], False, 2, (2, 4)),
        ([4, 2], True, 2, (4, 2)),
        ([4], True, 1, (4,)),
        ([2, 4], True, 0, (2, 4)),
    ],
)
def test_cherrypick_roll(rolls, best, quota, expected):
    assert cherrypick_roll(rolls=rolls, best=best, quota=quota) == expected


@pytest.mark.parametrize(
    "rolls, best, quota",
    [
        ([], True, 0),
        ([1, 3], True, 3),
        ([], True, 1),
    ],
)
def test_cherrypick_roll_value_error(rolls, best, quota):
    with pytest.raises(ValueError):
        cherrypick_roll(rolls=rolls, best=best, quota=quota)
