# -*- coding: utf-8 -*-
import pytest

from dddroller.class_roller import DiceRoll
from dddroller.parser import DDDiceRollerParser

INSTRUCTION_4D20B3: str = "4d20b3"
INSTRUCTION_100D100: str = "100d100"
VERBOSE: str = "--verbose"
VERBOSE_ALIASE: str = "-v"


@pytest.fixture
def roll_4d20b3() -> DiceRoll:
    return DiceRoll(INSTRUCTION_4D20B3)


@pytest.fixture
def roll_100d100() -> DiceRoll:
    return DiceRoll(INSTRUCTION_100D100)


@pytest.fixture
def high_roll() -> DiceRoll:
    roll: DiceRoll = DiceRoll(INSTRUCTION_4D20B3)
    setattr(roll, "_DiceRoll__rolls", [9, 20, 19, 2])
    setattr(roll, "_DiceRoll__picks", (20, 19, 9))
    return roll


@pytest.fixture
def low_roll() -> DiceRoll:
    roll: DiceRoll = DiceRoll(INSTRUCTION_4D20B3)
    setattr(roll, "_DiceRoll__rolls", [8, 18, 19, 1])
    setattr(roll, "_DiceRoll__picks", (8, 18, 19))
    return roll


@pytest.fixture
def parser():
    return DDDiceRollerParser().parser
