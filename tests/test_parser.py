# -*- coding: utf-8 -*-
import pytest
from argparse import ArgumentParser

from . import parser, INSTRUCTION_4D20B3, INSTRUCTION_100D100, VERBOSE, VERBOSE_ALIASE


@pytest.mark.parametrize("argument", [VERBOSE, VERBOSE_ALIASE, None,])
def test_dd_dice_roller_parser_verbose(argument, parser) -> None:  # noqa: F811
    if argument is None:
        # defualt is false
        assert not parser.parse_args([INSTRUCTION_4D20B3]).verbose
    else:
        assert parser.parse_args([argument, INSTRUCTION_4D20B3]).verbose


@pytest.mark.parametrize(
    "instructions, expected",
    [
        ([INSTRUCTION_4D20B3], [INSTRUCTION_4D20B3]),
        ([VERBOSE, INSTRUCTION_4D20B3], [INSTRUCTION_4D20B3]),
        ([INSTRUCTION_4D20B3, VERBOSE], [INSTRUCTION_4D20B3]),
        (
            [INSTRUCTION_100D100, INSTRUCTION_4D20B3],
            [INSTRUCTION_100D100, INSTRUCTION_4D20B3],
        ),
        (
            [VERBOSE, INSTRUCTION_100D100, INSTRUCTION_4D20B3],
            [INSTRUCTION_100D100, INSTRUCTION_4D20B3],
        ),
        (
            [INSTRUCTION_100D100, INSTRUCTION_4D20B3, VERBOSE],
            [INSTRUCTION_100D100, INSTRUCTION_4D20B3],
        ),
    ],
)
def test_dd_dice_roller_parser_instructions(
    instructions, expected, parser  # noqa: F811
) -> None:
    values = parser.parse_args(instructions).instructions
    assert isinstance(values, list)
    assert values == expected


@pytest.mark.parametrize(
    "instructions",
    [
        [
            INSTRUCTION_100D100,
            VERBOSE,  # flags inbetween instructions
            INSTRUCTION_4D20B3,
        ],
        [
            INSTRUCTION_100D100,
            VERBOSE_ALIASE,  # flags inbetween instructions
            INSTRUCTION_4D20B3,
        ],
        [],  # at least one instruction required
    ],
)
def test_dd_dice_roller_parser_error(instructions, parser) -> None:  # noqa: F811
    with pytest.raises(SystemExit):
        parser.parse_args(instructions)
