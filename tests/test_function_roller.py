# -*- coding: utf-8 -*-
import pytest

from dddroller.funtion_roller import dice_roll, DiceRoll


@pytest.mark.parametrize(
    "instruction, picks, rolls, modifier",
    [
        ("2d10+5", 2, 2, 5),
        ("d10-3", 1, 1, -3),
        ("4d6b3+1", 3, 4, 1),
        ("6d8w3-2", 3, 6, -2),
        ("1d6-3", 1, 1, -3),
        ("d6", 1, 1, 0),
        ("d100", 1, 1, 0),
        ("10d8-5", 10, 10, -5),
    ],
)
def test_roll_dice_instruction_string(
    instruction, picks, rolls, modifier
) -> None:  # noqa: F811
    roll: DiceRoll = dice_roll(instruction=instruction)
    assert roll.instruction == instruction
    assert len(roll.picks) == picks
    assert len(roll.rolls) == rolls
    assert roll.modifier == modifier


@pytest.mark.parametrize(
    "instruction, die, picks, quantity, modifier, best",
    [
        ("2d10+5", 10, 2, 2, 5, True),
        ("d10-3", 10, 1, 1, -3, True),
        ("4d6B3+1", 6, 3, 4, 1, True),
        ("6d8W3-2", 8, 3, 6, -2, False),
        ("d6-3", 6, 1, 1, -3, True),
        ("d6", 6, 1, 1, 0, True),
        ("d100", 100, 1, 1, 0, True),
        ("10d8-5", 8, 10, 10, -5, True),
    ],
)
def test_roll_dice_instruction_values(
    instruction, die, quantity, picks, modifier, best  # noqa: F811
) -> None:
    roll: DiceRoll = dice_roll(
        die=die, quantity=quantity, pick=picks, modifier=modifier, best=best
    )
    assert roll.instruction == instruction
    assert len(roll.picks) == picks
    assert len(roll.rolls) == quantity
    assert roll.modifier == modifier
    assert modifier < roll.value <= (die * picks + modifier)
