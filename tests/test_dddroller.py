# -*- coding: utf-8 -*-
from dddroller import __version__


def test_version():
    assert __version__ == "1.1.0"
