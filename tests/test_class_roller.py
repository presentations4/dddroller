# -*- coding: utf-8 -*-
import pytest

from dddroller.class_roller import DiceRoll
from dddroller import INSTRUCTION, MODIFIER, PICKS, ROLLS

from . import roll_4d20b3, high_roll, low_roll, roll_100d100


@pytest.mark.parametrize(
    "instruction",
    [
        "2d10+5",
        "d10-3",
        "4d6b3+1",
        "6d8w3-2",
        "1d6-3",
        "d6",
        "d100",
        "10d8-5",
    ],
)
def test_roll_object_instruction(instruction) -> None:
    roll = DiceRoll(instruction)
    assert str(roll) == instruction
    assert roll.instruction == instruction
    assert not roll.is_rolled
    assert not roll.picks
    assert not roll.rolls
    assert roll.value == roll.modifier
    assert isinstance(roll.modifier, int)
    assert not (roll.picks or roll.rolls)


def test_roll_object_call(roll_4d20b3) -> None:  # noqa: F811
    roll: DiceRoll = roll_4d20b3()
    assert isinstance(roll.picks, tuple)
    assert isinstance(roll.rolls, list)
    assert bool(roll.picks) and bool(roll.rolls)
    assert len(roll.rolls) == 4
    assert len(roll.picks) == 3
    assert sum(roll.rolls[:3]) <= sum(roll.picks)


def test_roll_object_conditional_operators(high_roll, low_roll) -> None:  # noqa: F811
    assert 44 < low_roll < high_roll
    assert 45 <= low_roll <= high_roll
    assert high_roll == high_roll
    assert high_roll == high_roll.value
    assert 50 > high_roll > low_roll
    assert 48 >= high_roll >= low_roll


@pytest.mark.parametrize(
    "other, expected_exception",
    [
        ([], TypeError),
        ({}, TypeError),
        ("", TypeError),
        (DiceRoll("d100"), ValueError),
    ],
)
def test_roll_object_conditional_operator_error(
    other, expected_exception, roll_4d20b3  # noqa: F811
) -> None:
    with pytest.raises(expected_exception):
        roll_4d20b3 < other
        roll_4d20b3 > other
        roll_4d20b3 >= other
        roll_4d20b3 <= other
        roll_4d20b3 == other


def test_roll_object_randomness(roll_100d100) -> None:  # noqa: F811
    first = roll_100d100()  # first roll
    first_rolls = first.rolls
    second = first()  # second roll
    assert first is second
    assert second.rolls != first_rolls
